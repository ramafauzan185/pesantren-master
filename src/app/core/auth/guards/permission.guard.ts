import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanDeactivate, CanLoad, Route, Router, RouterStateSnapshot, UrlSegment, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { PermissionsService } from '../../permissions/permissions.service';
import { switchMap, take } from 'rxjs/operators';

@Injectable({
  	providedIn: 'root'
})
export class PermissionGuard implements CanActivate, CanActivateChild, CanDeactivate<unknown>, CanLoad {

	/**
     * Constructor
     */
	constructor(
        private _permissionService: PermissionsService,
		private _router: Router
    )
    {
    }

  	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

		let permission = route.data.permission;
		return this._permissionService.get().pipe(
			switchMap((permissions: any) => {
				if(permissions.includes(permission)) {
					return of(true);
				}

				// Redirect to the home page
				this._router.navigateByUrl('/');
				return of(false);
			})
		);
	}

	canActivateChild(
		childRoute: ActivatedRouteSnapshot,
		state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return true;
	}

	canDeactivate(
		component: unknown,
		currentRoute: ActivatedRouteSnapshot,
		currentState: RouterStateSnapshot,
		nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		return true;
	}

	canLoad(
		route: Route,
		segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		
		return true;
	}
}
