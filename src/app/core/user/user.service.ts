import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { User } from './user.types';
import { environment } from '../../../environments/environment';
import axios from 'axios';

@Injectable({
    providedIn: 'root'
})
export class UserService
{
    
    private _user: ReplaySubject<User> = new ReplaySubject<User>(1);

    public profileInformation: any= []; //{} ,'', 1222, true, false 

    /**
     * Constructor
     */
    constructor(private _httpClient: HttpClient)
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for user
     *
     * @param value
     */
    set user(value: any)
    {
        // Store the value
        this._user.next(value);
    }

    get user$(): Observable<any>
    {
        return this._user.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get the current logged in user data
     */
    get(): Observable<any>
    {
        const token = window.localStorage.getItem(environment.access_token_identifier);
        return this._httpClient.get(`${environment.api_url}/user-data`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        }).pipe(
            tap((data) => {
               // console.log(data);
                this.profileInformation = data.user;
                this._user.next(data.user);
            })
        );
    }

    /**
     * Update the user
     *
     * @param user
     */
    update(user: any): Observable<any>
    {
        return this._httpClient.patch<any>('api/common/user', {user}).pipe(
            map((response) => {
                this._user.next(response);
            })
        );
    }
}
