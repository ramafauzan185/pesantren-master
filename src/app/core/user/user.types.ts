export interface User
{
    id: string;
    name: string;
    email: string;
    username: string;
    avatar?: string;
    status?: string;
    first_name: string;
    check_week_calendar?: string;
}
