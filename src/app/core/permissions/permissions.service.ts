import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, pipe, ReplaySubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  	providedIn: 'root'
})
export class PermissionsService {

  	private _permissions: ReplaySubject<any> = new ReplaySubject<any>(1);

	/**
	 * Constructor
	 */
	constructor(private _httpClient: HttpClient)
	{
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Accessors
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Getter for navigation
	 */
	get permissions$(): Observable<any>
	{
		return this._permissions.asObservable();
	}

	// -----------------------------------------------------------------------------------------------------
	// @ Public methods
	// -----------------------------------------------------------------------------------------------------

	/**
	 * Get all navigation data
	 */
	get(): Observable<any>
	{
		const token = window.localStorage.getItem(environment.access_token_identifier);
		return this._httpClient.get(`${environment.api_url}/setup/permissions`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		}).pipe(
			tap((permissions) => {
				this._permissions.next(permissions);
			})
		);
	}
}
